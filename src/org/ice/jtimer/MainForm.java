package org.ice.jtimer;

import java.awt.EventQueue;
import java.io.FileNotFoundException;

import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import org.tlib.config.EasyConfig;

public class MainForm {
	private final String settingsFile="settings.ini";

	private JFrame frame;
	private EasyConfig easyConfig = null;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainForm window = new MainForm();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainForm() {
		initialize();
		try {
			easyConfig=new EasyConfig(settingsFile);
		} catch (FileNotFoundException e) {
			System.out.println("\n *** Err. MainFrame:MainFrame:EasyConfig - Exception occured. "+e.getMessage());
		}
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e) {
			System.out.println("\n *** Err. MainFrame:intialize - cannot close stream."+e.getMessage());
		} catch (InstantiationException e) {
			System.out.println("\n *** Err. MainFrame:intialize - cannot close stream."+e.getMessage());
		} catch (IllegalAccessException e) {
			System.out.println("\n *** Err. MainFrame:intialize - cannot close stream."+e.getMessage());
		} catch (UnsupportedLookAndFeelException e) {
			System.out.println("\n *** Err. MainFrame:intialize - cannot close stream."+e.getMessage());
		}

		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}
